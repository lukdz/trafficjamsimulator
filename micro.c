#include <stdio.h>
#include <math.h>

double max(double a, double b){
    if(a>b){
        return a;
    }
    else{
        return b;
    }
}

int main (int argc, char *argv[]){
    if (argc < 3){
        printf ("Jako argumenty programu proszę podać:\n");
        printf ("-przepustowość drogi\n");
        printf ("-średnie natężenie ruchu\n");
        printf ("OPCJONALNIE:\n");
        printf ("-godzinę od której zacząć wyświetlać wyniki\n");
        printf ("-co ile minut wyświetlać aktualny stan\n");
        return 0;
    }
    double capacity, load, jam=0, current;
    sscanf(argv[1], "%lf", &capacity);
    sscanf(argv[2], "%lf", &load);
    int hours=0, minutes=60;
    if (argc >= 4){
        sscanf(argv[3], "%d", &hours);
    }
    if (argc == 5){
        sscanf(argv[4], "%d", &minutes);

    }

    printf("User input for calculation\n");
    printf("\tconst capacity: %.2f\n", capacity);
    printf("\taverage load:\t%.2f\n", load);
    printf("Display settings\n");
    printf("\tstart at:\t%d:00\n", hours);
    printf("\tinterval:\t%d minutes\n", minutes);
    printf("Calculation results\n");
    printf("\t\tcurrent\t cars in\n");
    printf("\t h:min\ttraffic\t   jam\n");
    for(int i=0; i<24*60; i++){
        current = i;
        current = (sin((current/360-0.5)*M_PI)+1.0)*load;
        jam=max(0,jam+current-capacity);
        if(i>hours*60 && i%minutes == 0){
            printf("\t%2d:%.2d\t%6.2f\t%7.2f\n", i/60, i%60, current, jam);
        }
    }

}
