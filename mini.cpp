#include <queue>
#include <iostream>
#include <cstdlib>
#include <math.h>
#include <cstdio>

using namespace std;

double max(double a, double b){
    if(a>b){
        return a;
    }
    else{
        return b;
    }
}

int main(int argc, char **argv){
    if (argc < 3){
        cout << "Jako argumenty programu proszę podać:" << endl;
        cout << "-przepustowość drogi" << endl;
        cout << "-średnie natężenie ruchu" << endl;
        cout << "OPCJONALNIE:" << endl;
        cout << "-godzinę od której zacząć wyświetlać wyniki" << endl;
        cout << "-co ile minut wyświetlać aktualny stan" << endl;
        return 0;
    }

    double capacity, load, jam=0, current, wait_time;
    capacity = atof (argv[1]);
    load = atof (argv[2]);
    int hours=0, minutes=60;
    if (argc >= 4){
        hours = atoi(argv[3]);

    }
    if (argc == 5){
        minutes = atoi(argv[4]);

    }
    cout << "User input for calculation" << endl;
    cout << '\t' << "const capacity: " << capacity << endl;
    cout << '\t' << "average load: " << '\t' << load << endl;
    cout << "Display settings" << endl;
    cout << '\t' << "start at: " << '\t' << hours << ":00" << endl;
    cout << '\t' << "interval: " << '\t' << minutes << " minutes" << endl;
    cout << "Calculation results" << endl;
    cout << "\t\t" << "current" << '\t' << "cars in" << '\t' << "wait in" << endl;
    cout << '\t' << " h:min" << '\t' << "traffic" << '\t' << "  jam" << '\t' << "minutes" << endl;
    queue < int > kolejka;
    for(int i=0; i<24*60; i++){
        if(i%5 == 0){
            current = i;
            current = (sin((current/360-0.5)*M_PI)+1.0)*load;
            current = round(current);
            for(int j=0; j<=current*5; j++){
                kolejka.push( i );
            }
            jam = 0;
            wait_time = 0;
            for(int j=0; j<=capacity*5 && !kolejka.empty(); j++){
                jam++;
                wait_time += i-kolejka.front();
                kolejka.pop();
            }
            if(jam != 0){
                wait_time /= jam;
            }
        }
        if(i>hours*60 && i%minutes == 0){
            printf("\t%2d:%.2d\t%4.0f\t%5lu\t%5.0f\n",
                   i/60, i%60, current, kolejka.size(), round(wait_time));
        }
    }
}
